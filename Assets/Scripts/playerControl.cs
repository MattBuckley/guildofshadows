﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerControl : MonoBehaviour {

    public float speed;
    private Vector3 destination;
    private bool moving;

    // Use this for initialization
    void Start () {
        moving = false;
        destination = this.transform.position;
        Debug.Log("Initialised");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("Mouse Clicked");
            if (!moving)
            {
                moving = true;
                setDest();
            }
        }

        move();
    }

    void setDest()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, 100.0f))
        {
            destination = hit.point;
            destination.y = this.transform.position.y;
            Debug.Log("Position:" + this.transform.position);
            Debug.Log("Destination:" + destination);
        }
    }

    void move()
    {
        //Debug.Log("Moving");
        transform.position = Vector3.MoveTowards(transform.position, destination, Time.deltaTime * speed);

        if (this.transform.position == destination)
            moving = false;
    }
}
