﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MoveCharacter : MonoBehaviour
{

    Vector3 target;
    Vector3 start;
    NavMeshAgent agent;
    private bool movedThisTurn;
    private int stoppingDistance;

    // Use this for initialization
    void Start()
    {
        target = transform.position;
        start = transform.position;
        Debug.Log("Initial Dest: " + transform.position + " Target Dest: " + target);
        agent = GetComponent<NavMeshAgent>();
        movedThisTurn = false;
        stoppingDistance = 5;
    }

    // Update is called once per frame
    void Update()
    {
        if ((start != target) && (Vector3.Distance(target, transform.position) < stoppingDistance))
        {
            Debug.Log("Character: " + name + "start == target " + (start == target));
            movedThisTurn = true;
            start = target;
            Debug.Log("Character: " + name + "start == target " + (start == target));
        }
    }

    public void setDest()
    {
        if (isStationary() == true)
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 100.0f))
            {
                target = hit.point;
                target.y = transform.position.y;
            }

            agent.SetDestination(target);
        }
    }

    public bool isStationary() { return (target == start) && (Vector3.Distance(target, transform.position) < stoppingDistance); }
    public void setMovedThisTurn(bool hasMoved) { movedThisTurn = hasMoved; }
    public bool getMovedThisTurn() { return movedThisTurn; }
}
