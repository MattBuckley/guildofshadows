﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrganiseCharacters : MonoBehaviour {

    static GameObject[] player;
    GameObject currentCharacter;
    int characterIndex;
    public int AI_Layer;

	// Use this for initialization
	void Start () {
        player = GameObject.FindGameObjectsWithTag("Character");
        characterIndex = 0;
        currentCharacter = player[characterIndex];
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("charIndex: " + characterIndex);

            if (currentCharacter.GetComponent<MoveCharacter>().getMovedThisTurn() == false)
                if (currentCharacter.layer == AI_Layer)
                    currentCharacter.GetComponent<stateMachine>().activate();
                else
                    currentCharacter.GetComponent<MoveCharacter>().setDest();
        }

        if (currentCharacter.GetComponent<MoveCharacter>().getMovedThisTurn() == true)
        {
            Debug.Log(currentCharacter.name + "Has moved: " + currentCharacter.GetComponent<MoveCharacter>().getMovedThisTurn());
            Debug.Log("Index: " + characterIndex);
            characterIndex++;
            if (characterIndex < player.Length)
            {
                Debug.Log("Switching character to " + characterIndex);
                currentCharacter = player[characterIndex];
            }
            else
            {
                resetCharacters();
            }
        }
            
    }

    void resetCharacters()
    {
        Debug.Log("Resetting characters");
        foreach (GameObject character in player)
            character.GetComponent<MoveCharacter>().setMovedThisTurn(false);

        characterIndex = 0;
        currentCharacter = player[characterIndex];
        Debug.Log("charIndex: " + characterIndex);
    }
}
