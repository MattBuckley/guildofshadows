﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavMeshController : MonoBehaviour {

    Vector3 target;
    NavMeshAgent agent;
    private bool moving;

	// Use this for initialization
	void Start () {
        init();
        agent = GetComponent<NavMeshAgent>();        
	}

    void init()
    {
        moving = false;
        target = transform.position;
    }
	
	// Update is called once per frame
	void Update () {
        /*if (Input.GetMouseButtonDown(0))
            if(!moving)
                setDest();*/

        agent.SetDestination(target); //Does this need to be done every frame?

        if (target == transform.position)
            moving = false;
	}

    void setDest()
    {
        moving = true;
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, 100.0f))
        {
            target = hit.point;
            target.y = transform.position.y;
        }
    }

    bool isMoving() { return moving; }

}
