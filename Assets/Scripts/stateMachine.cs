﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class stateMachine : MonoBehaviour {

    public GameObject targetCharacter;
    NavMeshAgent agent;

    public State currentState;
    static stateMachine theMachine;

    // Use this for initialization
    void Start()
    {
        theMachine = this;
        currentState = new Chase();
        agent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void activate()
    {
        currentState.activate();

        GetComponent<MoveCharacter>().setMovedThisTurn(true);
    }

    abstract public class State
    {
        //Used to start the character's decision making and moves
        abstract public void activate();
        abstract public void attack();
        abstract public void move();
    }

    private class Patrol : State
    {
        override public void activate() { Debug.Log("Activated!"); }
        override public void attack() { }
        override public void move() { }
    }

    private class Chase : State
    {
        override public void activate()
        {
            Debug.Log("Chasing!");
            theMachine.agent.SetDestination(theMachine.targetCharacter.transform.position);
        }
        override public void attack() { }
        override public void move() { }
    }


}
